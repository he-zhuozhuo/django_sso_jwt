from django.apps import AppConfig


class SsoCenterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sso_center'
