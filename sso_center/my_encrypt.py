# 对密码进行哈希
import hashlib

from sso_jwt.settings import SECRET_KEY


def user_password_encrypt(username, password):
    new_hash = hashlib.sha1()
    new_hash.update((username + password + SECRET_KEY).encode('utf8'))
    return new_hash.hexdigest()
