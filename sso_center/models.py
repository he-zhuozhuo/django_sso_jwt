from datetime import datetime
from django.db import models
from django.db.models import CharField
from sso_center.my_encrypt import user_password_encrypt


class AppUser(models.Model):
    username = CharField(max_length=100, unique=True, verbose_name='用户名')
    password = CharField(max_length=255, default="111111", null=True, blank=True, verbose_name='密码')
    create_date = CharField(max_length=50, default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'), null=True, blank=True,)
    update_date = CharField(max_length=50, null=True, blank=True,)
    status = CharField(max_length=2, default=1, null=True, blank=True, verbose_name='0-未激活 1-活动 2-冻结 3-注销')
    error_num = CharField(max_length=2, default=0, null=True, blank=True, verbose_name='密码错误次数')
    remark = CharField(max_length=255, null=True, blank=True, verbose_name='备注')

    # def keys(self):
    #     return 'username', 'password', 'create_date', 'update_date', 'status', 'error_num', 'remark'

    def clean(self):
        self.create_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.update_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.password = user_password_encrypt(self.username, self.password)

    class Meta:
        db_table = 'app_user'
        verbose_name = '用户信息'
        ordering = ('create_date',)

